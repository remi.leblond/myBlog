import { Component, OnInit, OnDestroy } from '@angular/core';
import { BilletService } from '../services/Billet.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy {

  nbPosts: number;
  tempsConnexion: number;
  nbBilletSubscription: Subscription;

  constructor(private billetService: BilletService) {
  }

  ngOnInit() {
    this.nbBilletSubscription = this.billetService.nbPostSubject.subscribe(
      (nb: number) => {
        this.nbPosts = nb;
      }
    );
    this.billetService.emmitNbPostSubject();
  }

  ngOnDestroy() {
    this.nbBilletSubscription.unsubscribe();
  }

}
