import { Injectable, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';
import { Subject } from 'rxjs/Subject';
import { Billet } from '../models/Billet.model';

@Injectable()
export class BilletService {

  private billets: Billet[] = [];
  private billet: Billet;

  billetSubject = new Subject<any[]>();
  nbPostSubject = new Subject<number>();

  constructor() {
    // Création des données de départ
    this.billet = new Billet('test 1', 'Ceci est un test');
    this.billets.push(this.billet);
    this.billet = new Billet('test 2', 'Ceci est un test');
    this.billets.push(this.billet);
  }

  emmitPostsSubject() {
    this.billetSubject.next(this.billets.slice());
  }

  emmitNbPostSubject() {
    this.nbPostSubject.next(this.getNbPost());
  }

  createPost(newBillet: Billet) {
    this.billets.push(newBillet);
    this.emmitPostsSubject();
    this.emmitNbPostSubject();
  }

  // Compte le nombre de posts
  getNbPost() {
    return this.billets.length;
  }

  // Vote pour un billet
  like(index: number) {
    this.billets[index].like();
    this.emmitPostsSubject();
  }

  // Vote contre un billet
  dislike(index: number) {
    this.billets[index].dislike();
    this.emmitPostsSubject();
  }

  // Suppression d'un billet
  delete(index: number) {
    this.billets.splice(index, 1);
    this.emmitPostsSubject();
    this.emmitNbPostSubject();
  }

}
