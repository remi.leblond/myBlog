import { Component, OnInit, OnDestroy } from '@angular/core';
import { BilletService } from './services/Billet.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'myBlog';

  constructor() {}

  ngOnInit() {}

  ngOnDestroy() { }

}

