import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { Billet } from '../models/Billet.model';
import { BilletService } from '../services/Billet.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  listeBillets: Billet[] = [];
  billetsSubscription: Subscription;
  nbBilletSubscription: Subscription;
  nbPosts: number;

  constructor(private billetService: BilletService, private router: Router) {}

  ngOnInit() {
    this.billetsSubscription = this.billetService.billetSubject.subscribe(
      (appareils: any[]) => {
        this.listeBillets = appareils;
      }
    );
    this.nbBilletSubscription = this.billetService.nbPostSubject.subscribe(
      (nb: number) => {
        this.nbPosts = nb;
      }
    );
    this.billetService.emmitPostsSubject();
    this.billetService.emmitNbPostSubject();
    console.log('Création popListComponent');
  }

  ngOnDestroy() {
    this.billetsSubscription.unsubscribe();
    this.nbBilletSubscription.unsubscribe();
    console.log('Suppression postListComponent');
  }

}
