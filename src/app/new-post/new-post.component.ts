import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BilletService } from '../services/Billet.service';
import { Router } from '@angular/router';
import { Billet } from '../models/Billet.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private billetService: BilletService,
    private router: Router) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  onSubmitForm() {
    console.log('Validation du formulaire');
    const formValue = this.postForm.value;
    const newPost = new Billet(
      formValue['title'],
      formValue['content']
    );
    console.log('Enregistrement du billet');
    this.billetService.createPost(newPost);
    console.log('Navigation vers la liste');
    this.router.navigate(['posts']);
  }
}
