// Billet de blog
export class Billet {
  title: string;
  content: string;
  // Identifiant du post
  public id: number;
  // Date de création du billet
  private _createDate: Date;
  // Note finale
  private _loveRate: number;
  // Nombre de votes
  private _loveElt: number;

  constructor(title: string, content: string) {
    this.title = title;
    this.content = content;
    this._createDate = new Date();
    this._loveRate = 0;
    this.id = Math.floor(Math.random() * 100000000);
  }

  like() {
    this._loveRate++;
    this._loveElt++;
  }

  dislike() {
    this._loveRate--;
    this._loveElt++;
  }

  get LoveRate() {
    return this._loveRate;
  }

  get CreateDate() {
    return this._createDate;
  }

}
