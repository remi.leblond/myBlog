import { Component, Input, OnInit } from '@angular/core';
import { BilletService } from '../services/Billet.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss']
})
export class PostItemComponent {

  constructor(private billetService: BilletService) { }

  @Input() title: string ;
  @Input() content: string ;
  @Input() rate: number;
  @Input() dateRedac: Date;
  @Input() id: number;
  @Input() indexOfPost: number;

  onLike() {
    this.billetService.like(this.indexOfPost);
  }
  onDislike() {
    this.billetService.dislike(this.indexOfPost);
  }

  onDelete() {
    this.billetService.delete(this.indexOfPost);
  }

}
